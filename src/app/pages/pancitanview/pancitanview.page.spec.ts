import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PancitanviewPage } from './pancitanview.page';

describe('PancitanviewPage', () => {
  let component: PancitanviewPage;
  let fixture: ComponentFixture<PancitanviewPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PancitanviewPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PancitanviewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
